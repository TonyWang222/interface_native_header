/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_PATH_H
#define C_INCLUDE_DRAWING_PATH_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数.
 * 
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_path.h
 *
 * @brief 文件中定义了与自定义路径相关的功能函数
 *
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 函数用于创建一个路径对象
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的路径对象
 * @since 8
 * @version 1.0
 */
OH_Drawing_Path* OH_Drawing_PathCreate(void);

/**
 * @brief 函数用于销毁路径对象并回收该对象占有的内存
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 参数为一个指向路径对象的指针
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathDestroy(OH_Drawing_Path*);

/**
 * @brief 函数用于设置自定义路径的起始点位置
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 参数为一个指向路径对象的指针
 * @param x 参数为起始点的横坐标
 * @param y 参数为起始点的纵坐标
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathMoveTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief 函数用于添加一条从路径的最后点位置到目标点位置的线段
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 参数为一个指向路径对象的指针
 * @param x 参数为目标点的横坐标
 * @param y 参数为目标点的纵坐标
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathLineTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief 函数用于给路径添加一段弧线，绘制弧线的方式为角度弧，该方式首先会指定一个矩形边框，矩形边框会包裹椭圆，
 * 然后会指定一个起始角度和扫描度数，从起始角度扫描截取的椭圆周长一部分即为绘制的弧线。另外会默认添加一条从路径的最后点位置到弧线起始点位置的线段
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 参数为一个指向路径对象的指针
 * @param x1 参数为包围椭圆的矩形左上角点位置的横坐标
 * @param y1 参数为包围椭圆的矩形左上角点位置的纵坐标
 * @param x2 参数为包围椭圆的矩形右下角点位置的横坐标
 * @param y2 参数为包围椭圆的矩形右下角点位置的纵坐标
 * @param startDeg 参数为起始的角度
 * @param sweepDeg 参数为扫描的度数
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathArcTo(OH_Drawing_Path*, float x1, float y1, float x2, float y2, float startDeg, float sweepDeg);

/**
 * @brief 函数用于添加一条从路径最后点位置到目标点位置的二阶贝塞尔圆滑曲线
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 参数为一个指向路径对象的指针
 * @param ctrlX 参数为控制点位置的横坐标
 * @param ctrlY 参数为控制点位置的纵坐标
 * @param endX 参数为目标点位置的横坐标
 * @param endY 参数为目标点位置的纵坐标
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathQuadTo(OH_Drawing_Path*, float ctrlX, float ctrlY, float endX, float endY);

/**
 * @brief 函数用于添加一条从路径最后点位置到目标点位置的三阶贝塞尔圆滑曲线
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 参数为一个指向路径对象的指针
 * @param ctrlX1 参数为第一个控制点位置的横坐标
 * @param ctrlY1 参数为第一个控制点位置的纵坐标
 * @param ctrlX2 参数为第二个控制点位置的横坐标
 * @param ctrlY2 参数为第二个控制点位置的纵坐标
 * @param endX 参数为目标点位置的横坐标
 * @param endY 参数为目标点位置的纵坐标
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathCubicTo(
    OH_Drawing_Path*, float ctrlX1, float ctrlY1, float ctrlX2, float ctrlY2, float endX, float endY);

/**
 * @brief 函数用于闭合路径，会添加一条从路径起点位置到最后点位置的线段
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 参数为一个指向路径对象的指针
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathClose(OH_Drawing_Path*);

/**
 * @brief 函数用于重置自定义路径数据
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 参数为一个指向路径对象的指针
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathReset(OH_Drawing_Path*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif